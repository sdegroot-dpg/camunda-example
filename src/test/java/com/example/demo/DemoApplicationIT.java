package com.example.demo;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.assertThat;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationIT {

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private ProcessEngine processEngine;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void test() {

        ProcessInstance processInstance = runtimeService
            .startProcessInstanceByKey("dunning_process", "A1");

        assertThat(processInstance).isStarted()
            .isWaitingAt("ExclusiveGateway_0uv188w");

        runtimeService.correlateMessage("aria_dunning_completed", "A1");

        assertThat(processInstance).hasPassed("EndEvent_1dkxwoj").isEnded();

    }

    @Test
    public void test2() throws InterruptedException {

        ProcessInstance processInstance = runtimeService
            .startProcessInstanceByKey("dunning_process", "A1");

        assertThat(processInstance).isStarted()
            .isWaitingAt("ExclusiveGateway_0uv188w");

        runtimeService.correlateMessage("aria_dunning_sommation_date_passed", "A1");

        assertThat(processInstance).isWaitingAt("ExclusiveGateway_0npbqdr");


    }


}