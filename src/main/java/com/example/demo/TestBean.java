package com.example.demo;

import org.springframework.stereotype.Component;

@Component(value = "testBean")
public class TestBean {


    public void sayHello() {
        System.out.println("Hello!");
    }
}
