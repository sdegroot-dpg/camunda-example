# Camunda Example

- Embedded Camunda Engine
- Runs on in-memory H2
- Testing library included
- Java 11

## Running

Start the application using:

```
mvn spring-boot:run
```

Goto http://localhost:8080 (admin/admin)

Any `bpmn/cmmn/dmn` files listed in `src/main/resources/processes` will be automatically deployed when starting this application.